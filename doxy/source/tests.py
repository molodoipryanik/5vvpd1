from vvpd import check_input
from vvpd import find_wrong_letters
import pytest


def test_correct_input():
    """Прекрасно рабочий вариант ввода"""
    assert check_input('AASSDDFF') == 'AASSDDFF'


def test_wrong_letters_input():
    """Строчные буквы"""
    with pytest.raises(ValueError):
        check_input('dsadwsdaASDW')


def test_another_symbols_input():
    """Прочие символы"""
    with pytest.raises(ValueError):
        check_input('%$@(%@!')


def test_numbers_input():
    """Цифры"""
    with pytest.raises(ValueError):
        check_input('13224312')


def test_empty_input():
    """Пустая строка"""
    with pytest.raises(ValueError):
        check_input('')


def test_1_letter():
    """1 сломанная буква"""
    assert find_wrong_letters('AASDFG') == 'A'


def test_2_letters():
    """2 сломанные буквы"""
    assert find_wrong_letters('AASSDFG') == 'AS'


def test_3_letters():
    """3 сломанные буквы"""
    assert find_wrong_letters('AASSDDFG') == 'ASD'


def test_0_letters():
    """Отсутствие сломанных букв"""
    assert find_wrong_letters('ASDFG') == 'Broken characters not found'


def test_2_2_letters():
    """1 сломанная буква в середине слова"""
    assert find_wrong_letters('AASSDFGA') == 'S'


def test_2_3_letters():
    """Отсутствие сломанных букв"""
    assert find_wrong_letters('FFAAFA') == 'Broken characters not found'


def test_2_4_letters():
    """Отсутствие сломанных букв"""
    assert find_wrong_letters('AFAAFF') == 'Broken characters not found'
