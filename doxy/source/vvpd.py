﻿"""@package docstring
Практическая работа по ВВПД №5 Балябина Павла КИ19-17/1б
"""


def check_input(word):
    """Функция нужна для проверки корректности ввода строки данных.

    Args:
    word: слово, проверяемое на сломанные буквы.
    Returns:
    Входные данные
    Raises:
    ValueError
    Examples:
    >>> check_input('AASSDDFF')\n
    'AASSDDFF'\n
    >>> check_input('ASDF')\n
    'ASDF'\n
    >>> check_input('')\n
    Traceback (most recent call last):\n
        ...\n
    ValueError\n
    >>> check_input('@#$$!')\n
    Traceback (most recent call last):\n
        ...\n
    ValueError\n
    >>> check_input('') \n
    Traceback (most recent call last): \n
        ...\n
    ValueError
    """
    list_of_letters_check = list(word)
    if len(list_of_letters_check) == 0:
        raise ValueError()
    for check_word in list_of_letters_check:
        if ord(check_word) < 65 or ord(check_word) > 90:
            raise ValueError()
    return word


def find_wrong_letters(word):
    """Функция нужна для нахождения сломанных букв, если таковые имеются.\n
    Сломанными считаются буквы, которые дублируются.
    Args:
    word: слово, которое проверяют на сломанные буквы.
    Returns:
    Последовательность сломанных букв.\n
    Examples:
    >>> find_wrong_letters('AASDFG')\n
    'A'\n
    >>> find_wrong_letters('ASDFG')\n
    'Broken characters not found'\n
    >>> find_wrong_letters('AASSDDFG')\n
    'ASD'\n
    >>> find_wrong_letters('FFAAFA')\n
    'Broken characters not found'
    """
    broken = ''
    list_of_letters = list(word)
    if len(list_of_letters) > 1:
        for index in range(len(set(list_of_letters))):
            letter_buff, list_of_letters[index] = (list_of_letters[index],
                                                   'letter')
            while list_of_letters.count(letter_buff) > 0:
                list_of_letters.remove(letter_buff)
            list_of_letters[index] = letter_buff
    for letter in list_of_letters:
        if word.count(letter) % 2 == 0:
            count_of_occurrences = word.count(letter * 2)
            if count_of_occurrences == word.count(letter) / 2:
                broken += letter
    if broken == '':
        broken = 'Broken characters not found'
    return broken


def main():
    """Функция одновременно проверяет входные данные и выводит результат.
    Returns:
    Последовательность сломанных букв, если таковые имеются,\n
    иначе выводит сообщение об отсутствии таковых букв.
    Examples:
    >>> main()\n
    >>> 'AASSD'\n
    'AS'\n
    >>> main()\n
    >>> 'WETRSO'\n
    'Broken characters not found'\n
    >>> main()\n
    >>> 'OYYEEEE'\n
    'YE'
    """
    print(find_wrong_letters(check_input(input())))


if __name__ == '__main__':
    main()
